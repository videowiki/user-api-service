const User = require('./models').User;
const async = require('async');

const { organizationService } = require('./services')

module.exports = {
    getUserByEmail(email) {
        return new Promise((resolve, reject) => {
            User.findOne({ email })
                .select('+organizationRoles.inviteToken')
                .then((userData) => {
                    if (!userData) return resolve(null)
                    userData = userData.toObject();
                    const fetchOrgFuncArray = [];
                    userData.organizationRoles.forEach((role) => {
                        fetchOrgFuncArray.push(cb => {
                            organizationService.findById(role.organization)
                                .then((organization) => {
                                    role.organization = organization;
                                    cb();
                                })
                                .catch(err => {
                                    console.log(err);
                                    return cb();
                                })
                        })
                    })
                    async.parallelLimit(fetchOrgFuncArray, 2, () => {
                        return resolve(userData);
                    })
                })
                .catch(reject)
        })
    }
}