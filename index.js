const { server, app, createRouter } = require('./generateServer')();
const controller = require('./controller');
const videowikiGenerators = require('@videowiki/generators');
const VW_SUPER_TRANSCRIBERS_EMAILS = process.env.VW_SUPER_TRANSCRIBERS_EMAILS && process.env.VW_SUPER_TRANSCRIBERS_EMAILS.split(',').length > 0 ? process.env.VW_SUPER_TRANSCRIBERS_EMAILS.split(',').map(r => r.trim()).filter(r => r) : [];

const mongoose = require('mongoose');

const DB_CONNECTION = process.env.USER_SERVICE_DATABASE_URL;

let mongoConnection;
mongoose.connect(DB_CONNECTION)
.then((con) => {
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })

    videowikiGenerators.healthcheckRouteGenerator({ router: app, mongoConnection })

    const User = require('./models').User;
    console.log('super transcribers', VW_SUPER_TRANSCRIBERS_EMAILS)
    
    VW_SUPER_TRANSCRIBERS_EMAILS.forEach(email => {
        User.findOneAndUpdate({ email }, { $set: { superTranscriber: true } })
            .then(() => {
                console.log(email, ' is set as super transcriber')
            })
            .catch(err => {
                console.log(err)
            })
    })

    app.use('/db', require('./dbRoutes')(createRouter()))

    app.all('*', (req, res, next) => {
        if (req.headers['vw-user-data']) {
            try {
                const user = JSON.parse(req.headers['vw-user-data']);
                req.user = user;
            } catch (e) {
                console.log(e);
            }
        }
        next();
    })

    app.get('/', controller.getAll)
    app.get("/getOrgUsers", controller.getOrgUsers);
    app.get("/count", controller.getOrgUsersCounts);
    app.get("/getUserDetails", controller.getUserDetails);
    app.get("/isValidToken", controller.isValidToken);

    app.patch('/showUserGuiding', controller.updateShowUserGuiding)
    app.patch('/showCuttingTutorial', controller.updateShowCuttingTutorial)
    app.patch('/showProofreadingTutorial', controller.updateShowProofreadingTutorial)
    app.patch('/showTranslatingTutorial', controller.updateShowTranslatingTutorial)

    app.patch('/:userId/password', controller.updatePassword);
    app.post('/resetPassword', controller.resetPassword);
    app.get('/:id', controller.getById)


})
.catch(err => {
    console.log('error connecting to database', err);
    process.exit(1);
})


const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
