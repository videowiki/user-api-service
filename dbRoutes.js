const User = require('./models').User;
const utils = require('./utils');

module.exports = router => {

    router.get('/count', (req, res) => {
        console.log('hello count')
        User.count(req.query)
            .then(count => res.json(count))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message)
            })

    })


    router.get('/by_email', (req, res) => {
        const { email } = req.query;

        utils.getUserByEmail(email)
            .then((userData) => {
                return res.json(userData);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send('Something went wrong');
            })
    })
    // CRUD routes
    router.get('/', (req, res) => {
        const { sort, skip, limit, one, select, ...rest } = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }
        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        console.log(req.query)
        if (one) {
            q = User.findOne(rest);
        } else {
            q = User.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        if (select) {
            let selectStr = '';
            if (Array.isArray(select)) {
                select.forEach(item => {
                    selectStr += `+${item} `;
                })
            } else {
                selectStr += `+${select}`
            }
            q.select(selectStr);
        }
        q.then((users) => {
            return res.json(users);
        })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.post('/', (req, res) => {
        const data = req.body;
        User.create(data)
            .then((user) => {
                return res.json(user);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        User.update(conditions, { $set: values }, { ...options, multi: true })
            .then(() => User.find(conditions))
            .then(users => {
                return res.json(users);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/', (req, res) => {
        let conditions = req.body;
        let users;
        User.find(conditions)
            .then((a) => {
                users = a;
                return User.remove(conditions)
            })
            .then(() => {
                return res.json(users);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.get('/:id', (req, res) => {
        User.findById(req.params.id)
            .then((user) => {
                return res.json(user);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        User.findByIdAndUpdate(id, { $set: changes })
            .then(() => User.findById(id))
            .then(user => {
                return res.json(user);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/:id', (req, res) => {
        const { id } = req.params;
        let deletedUser;
        User.findById(id)
            .then(user => {
                deletedUser = user;
                return User.findByIdAndRemove(id)
            })
            .then(() => {
                return res.json(deletedUser);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })


    return router;
}