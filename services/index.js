const AUTH_SERVICE_API_ROOT = process.env.AUTH_SERVICE_API_ROOT;
const ORGANIZATION_SERVICE_API_ROOT = process.env.ORGANIZATION_SERVICE_API_ROOT;

const authService = require('@videowiki/services/auth')(AUTH_SERVICE_API_ROOT);
const organizationService = require('@videowiki/services/organization')(ORGANIZATION_SERVICE_API_ROOT);

module.exports = {
    authService,
    organizationService,
}
